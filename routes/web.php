<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('task.index');
});

Route::prefix('task')->group(function () {
    Route::get('/', [TaskController::class, 'index'])->name('task.index');
    Route::get('/create', [TaskController::class, 'create'])->name('task.create');
    Route::post('/store', [TaskController::class, 'store'])->name('task.store');
    Route::get('/edit/{task}', [TaskController::class, 'edit'])->name('task.edit');
    Route::put('/update/{task}', [TaskController::class, 'update'])->name('task.update');
    Route::get('/show/{task}', [TaskController::class, 'show'])->name('task.show');
    Route::delete('/destroy/{task}', [TaskController::class, 'destroy'])->name('task.destroy');
});