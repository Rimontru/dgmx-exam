<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('task.index', compact('tasks'));
    }

    public function create()
    {
        return view('task.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'due_date' => 'required',
        ]);

        $request->request->add(['user_id' => rand(1, 10)]);

        Task::create($request->all());

        return redirect()->route('task.index')->with('success', 'Task created successfully.');
    }

    public function edit(Task $task)
    {
        return view('task.edit', compact('task'));
    }

    public function update(Request $request, Task $task)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
            'completed' => 'required',
        ]);

        $request->request->add(['completed_at' => $request->completed ? now() : null]);


        $task->update($request->all());

        return redirect()->route('task.index')->with('success', 'Task updated successfully.');
    }

    public function show(Task $task)
    {
        return view('task.show', compact('task'));
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return redirect()->route('task.index')->with('success', 'Task deleted successfully.');
    }
}
