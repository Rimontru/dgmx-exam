@extends('layout.app')
@section('content')
    <br>
    <h1 class="text-center">Show Task</h1>
    <br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('task.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Due Date</th>
                            <th>Status</th>
                            <th>Completed</th>
                            <th>Completed At</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $task->id }}</td>
                            <td>{{ $task->title }}</td>
                            <td>{{ $task->description }}</td>
                            <td>To {{ \Carbon\Carbon::parse($task->due_date)->format('m/d/Y') }}</td>
                            <td>
                                @if($task->status == 0) Not Started @endif
                                @if($task->status == 1) On Going @endif
                                @if($task->status == 2) Completed @endif
                            </td>
                            <td>{{ ($task->completed)? 'Yes' : 'No' }}</td>
                            <td>{{ $task->completed_at? \Carbon\Carbon::parse($task->completed_at)->format('m/d/Y') : '-' }}</td>                                
                        </tr>                            
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection