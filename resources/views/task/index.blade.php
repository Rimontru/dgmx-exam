@extends('layout.app')
@section('content')
    <br>
    <h1 class="text-center">All Tasks</h1>
    <br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('task.create') }}" class="btn btn-primary">Create Task</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Due Date</th>
                            <th>Status</th>
                            <th>Completed</th>
                            <th>Completed At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ $task->id }}</td>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->description }}</td>
                                <td>To {{ \Carbon\Carbon::parse($task->due_date)->format('m/d/Y') }}</td>
                                <td>
                                    @if($task->status == 0) Not Started @endif
                                    @if($task->status == 1) On Going @endif
                                    @if($task->status == 2) Completed @endif
                                </td>
                                <td>{{ ($task->completed)? 'Yes' : 'No' }}</td>
                                <td>{{ $task->completed_at? \Carbon\Carbon::parse($task->completed_at)->format('m/d/Y') : '-' }}</td>
                                <td>
                                    <a href="{{ route('task.edit', $task->id) }}" class="btn btn-warning">Edit</a>
                                    <a href="{{ route('task.show', $task->id) }}" class="btn btn-info">Show</a>
                                    <form action="{{ route('task.destroy', $task->id) }}" method="POST" style="display: inline;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>                            
                        @endforeach                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection