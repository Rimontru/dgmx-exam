@extends('layout.app')
@section('content')
    <br>
    <h1 class="text-center">Edit Task</h1>
    <br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('task.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <form action="{{ route('task.update', $task->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group
                    @if($errors->has('title'))
                        has-error
                    @endif">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{ $task->title }}">
                    @if($errors->has('title'))
                        <span class="help-block
                        @if($errors->has('title'))
                            has-error
                        @endif">
                            {{ $errors->first('title') }}
                        </span>
                    @endif
                </div>
                <br>
                <div class="form-group
                    @if($errors->has('description'))
                        has-error
                    @endif">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control">{{ $task->description }}</textarea>
                    @if($errors->has('description'))
                        <span class="help-block
                        @if($errors->has('description'))
                            has-error
                        @endif">
                            {{ $errors->first('description') }}
                        </span>
                    @endif
                </div>
                <br>                
                <div class="form-group
                    @if($errors->has('status'))
                        has-error
                    @endif">
                    <label for="status">Status</label>
                    <select name="status" class="form-control">
                        <option value="0" @if($task->status == 0) selected @endif>Not Started</option>
                        <option value="1" @if($task->status == 1) selected @endif>On Going</option>
                        <option value="2" @if($task->status == 2) selected @endif>Completed</option>
                    </select>
                    @if($errors->has('status'))
                        <span class="help-block
                        @if($errors->has('status'))
                            has-error
                        @endif">
                            {{ $errors->first('status') }}
                        </span>
                    @endif
                </div>
                <br>
                <br>
                <div class="form-group
                    @if($errors->has('completed'))
                        has-error
                    @endif">
                    <label for="completed">Task was completed?</label>
                    <select name="completed" class="form-control">
                        <option value="0" @if(old('completed') == 0) selected @endif>No</option>
                        <option value="1" @if(old('completed') == 1) selected @endif>Yes</option>
                    </select>
                    @if($errors->has('completed'))
                        <span class="help-block
                        @if($errors->has('completed'))
                            has-error
                        @endif">
                            {{ $errors->first('completed') }}
                        </span>
                    @endif  
                </div>

                <br>
                <button type="submit" class="btn btn-success">Update</button>
            </form>
        </div>
    </div>
@endsection