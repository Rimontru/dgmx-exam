@extends('layout.app')
@section('content')
    <h1 class="text-center">Create New Task</h1>
    <br>

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('task.index') }}" class="btn btn-primary">Back</a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <form action="{{ route('task.store') }}" method="POST">
                @csrf
                <div class="form-group
                    @if($errors->has('title'))
                        has-error
                    @endif">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                    @if($errors->has('title'))
                        <span class="help-block
                        @if($errors->has('title'))
                            has-error
                        @endif">
                            {{ $errors->first('title') }}
                        </span>
                    @endif
                </div>
                <br>
                <div class="form-group
                    @if($errors->has('description'))
                        has-error
                    @endif">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control">{{ old('description') }}</textarea>
                    @if($errors->has('description'))
                        <span class="help-block
                        @if($errors->has('description'))
                            has-error
                        @endif">
                            {{ $errors->first('description') }}
                        </span>
                    @endif
                </div>
                <br>
                <div class="form-group
                    @if($errors->has('due_date'))
                        has-error
                    @endif">
                    <label for="due_date">Due Date</label>
                    <input type="date" name="due_date" class="form-control" value="{{ old('due_date') }}">
                    @if($errors->has('due_date'))
                        <span class="help-block
                        @if($errors->has('due_date'))
                            has-error
                        @endif">
                            {{ $errors->first('due_date') }}
                        </span>
                    @endif
                </div>
                <br>
                <button type="submit" class="btn btn-success">Save</button>
            </form>
        </div>
    </div>

@endsection